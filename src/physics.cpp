//==============================================================================
/*
 *  Game - MMO!
 *
 *  Ben Kelly
 */
//==============================================================================

#include "physics.h"
#include "game.h"

//------------------------------------------------------------------------------
Vec2::Vec2()
{
    x = 0;
    y = 0;
}

//------------------------------------------------------------------------------
Vec2::Vec2(double _x, double _y)
{
    x = _x;
    y = _y;
}

//------------------------------------------------------------------------------
Vec2 Vec2::operator+(const Vec2 &other)
{
    Vec2 result;
    result.x = x + other.x;
    result.y = y + other.y;
    return result;
}

//------------------------------------------------------------------------------
Vec2 Vec2::operator*(double scalar)
{
    Vec2 result;
    result.x = x * scalar;
    result.y = y * scalar;
    return result;
}

//------------------------------------------------------------------------------
Vec2 Vec2::DistTo(Vec2 target)
{
    return Vec2(target.x - x, target.y - y);
}

//------------------------------------------------------------------------------
bool Vec2::Inside(Vec2 prev, Vec2 next)
{
    bool inside = false;
    if ((prev.x <= x && x < next.x) || (next.x <= x && x <= prev.x)) inside = true;
    if ((prev.y <= y && y < next.y) || (next.y <= y && y <= prev.y)) inside = true;
    return inside;
}

//------------------------------------------------------------------------------
const Vec2 Vec2::ZERO(0, 0);

//------------------------------------------------------------------------------
PhysicsComponent::PhysicsComponent(CollisionGroup colGroup)
{
    mEng            = Game::GetInstance()->GetPhysicsEngine();
    mEng->RegisterComponent(this, colGroup);

    mPos            = Vec2::ZERO;
    mThr            = Vec2::ZERO;
    mVel            = Vec2::ZERO;
    mGrv            = Vec2::ZERO;
    mBounce         = 1.0;

}

//------------------------------------------------------------------------------
Vec2 PhysicsComponent::GetPosition()
{
    return mPos;
}

//------------------------------------------------------------------------------
Vec2 PhysicsComponent::GetThrust()
{
    return mThr;
}

//------------------------------------------------------------------------------
Vec2 PhysicsComponent::GetVelocity()
{
    return mVel;
}

//------------------------------------------------------------------------------
Vec2 PhysicsComponent::GetGravity()
{
    return mGrv;
}

//------------------------------------------------------------------------------
double PhysicsComponent::GetBounce()
{
    return mBounce;
}

//------------------------------------------------------------------------------
void PhysicsComponent::SetPosition(Vec2 pos)
{
     mPos = pos;
}

//------------------------------------------------------------------------------
void PhysicsComponent::SetThrust(Vec2 thrust)
{
    mThr = thrust;
}

//------------------------------------------------------------------------------
void PhysicsComponent::SetVelocity(Vec2 vel)
{
    mVel = vel;
}

//------------------------------------------------------------------------------
void PhysicsComponent::SetGravity(Vec2 gravity)
{
    mGrv = gravity;
}

//------------------------------------------------------------------------------
void PhysicsComponent::SetBounce(double bounce)
{
    mBounce = bounce;
}

//------------------------------------------------------------------------------
void PhysicsComponent::Update(int ms)
{
    // Update Velocity
    mVel.x += mThr.x * (ms / 1000.0f);
    mVel.y += mThr.y * (ms / 1000.0f);
    mVel.x += mGrv.x * (ms / 1000.0f);
    mVel.y += mGrv.y * (ms / 1000.0f);
    printf("Grav: %f, Vel: %f\n", mGrv.y, mVel.y);

    mPos = IntegrateEuler(ms);
}

//------------------------------------------------------------------------------
Vec2 PhysicsComponent::IntegrateEuler(int ms)
{
    Vec2 newPos;
    newPos = mPos + (mVel * (ms / 1000.0f));
    if (newPos.y > 307) {
        newPos.y = 307;
        mVel.y = -mVel.y * mBounce;
    }
    printf("YPOS: %f\n\n", newPos.y);
    return newPos;
}

//------------------------------------------------------------------------------
PhysicsEngine::PhysicsEngine()
{

}

//------------------------------------------------------------------------------
void PhysicsEngine::RegisterComponent(PhysicsComponent* component, CollisionGroup colGroup)
{

}
