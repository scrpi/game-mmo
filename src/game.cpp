//==============================================================================
/*
 *  Game - MMO!
 *
 *  Ben Kelly
 */
//==============================================================================
#include "game.h"

//------------------------------------------------------------------------------
bool Game::InitOpenGL()
{

    //mSDLGLContext = SDL_GL_CreateContext(mWindow);

    return true;
}

//------------------------------------------------------------------------------
bool Game::Init()
{
    mWindowWidth    = 1024;
    mWindowHeight   = 768;
    mRunning        = true;

    // SFML
    sf::VideoMode vm(mWindowWidth, mWindowHeight, 32);
    mWindow.create(vm, "My window");

    mClock.restart();
    mFPSClock.restart();

    mFont.loadFromFile("fonts/DejaVuSansMono.ttf");
    // TODO error checking

    if (!mBGTex.loadFromFile("bg.png")) {
        printf("Unable to load bg texture\n");
    }
    mBGRect.setPosition(0,0);
    mBGRect.setSize(sf::Vector2f(mWindowWidth, mWindowHeight));
    mBGRect.setTexture(&mBGTex, true);
    // Error Checking

    mConsole = new Console();

    mFPS = 0;

    return true;
}

//------------------------------------------------------------------------------
void Game::RenderFPS()
{
    if (mFPSClock.getElapsedTime().asMilliseconds() > GAME_FPS_CALC_INTERVAL) {
        mFPSClock.restart();
        mFPS = mFrameCount * (1000.0 / (float)GAME_FPS_CALC_INTERVAL);
        mFrameCount = 0;
    }
    mFrameCount++;

    if (mFPS > 0) {
        char fpsText[50];
        sprintf(fpsText, "FPS: %d", mFPS);

        sf::Text text;
        text.setFont(mFont);
        text.setCharacterSize(GAME_FONT_SIZE);
        text.setColor(sf::Color::White);
        text.setString(fpsText);
        sf::Rect<float> textRect = text.getLocalBounds();
        text.setPosition(mWindowWidth - 10 - textRect.width, 10);
        mWindow.draw(text);
    }
}

//------------------------------------------------------------------------------
void Game::OnEvent(sf::Event event)
{
    if (mConsole->OnEvent(event))
        return;

    if (event.type == sf::Event::Closed) {
        mRunning = false;
        return;
    }
    if (event.type == sf::Event::KeyPressed) {
        switch (event.key.code) {
        case sf::Keyboard::Escape:
            mRunning = false;
            return;
        case sf::Keyboard::Tilde:
            mConsole->Toggle();
            break;
        }
    }
}

//------------------------------------------------------------------------------
void Game::Update(float dt)
{
    mConsole->Update(dt);
    mCurrentRealm->Update(dt);
}

//------------------------------------------------------------------------------
void Game::Render(float dt)
{
    mWindow.clear(sf::Color::Black);
    mWindow.draw(mBGRect);

    RenderFPS();
    mCurrentRealm->Render(dt, &mWindow);
    mConsole->Render(dt, &mWindow);

    mWindow.display();
}

//------------------------------------------------------------------------------
void Game::Loop()
{
    sf::Clock loopTimer;
    sf::Event event;
    sf::Time  elapsed;
    sf::Time  renderLag;
    sf::Time  gameTick = sf::seconds(1.0 / (float)GAME_TICKS_PER_SECOND);

    loopTimer.restart();
    while (mRunning) {
        elapsed = loopTimer.getElapsedTime();
        loopTimer.restart();
        renderLag += elapsed;

        while (mWindow.pollEvent(event)) {
            OnEvent(event);
        }

        while (renderLag > gameTick) {
            renderLag -= gameTick;
            Update(gameTick.asSeconds());
        }

        Render(renderLag.asSeconds());
    }
}

//------------------------------------------------------------------------------
void Game::Cleanup()
{
    //TextureBank::Cleanup();

    /**
    if(mRenderer) {
        SDL_DestroyRenderer(mRenderer);
        mRenderer = NULL;
    }

    if(mWindow) {
        SDL_DestroyWindow(mWindow);
        mWindow = NULL;
    }

    IMG_Quit();
    SDL_Quit();
    */

    //delete mConsole;
    //delete mPhysicsEngine;
}

//------------------------------------------------------------------------------
int Game::Execute(int argc, char* argv[])
{
    if(!Init()) return 1;

    mCurrentRealmID = 0;
    mCurrentRealm = new Realm(0);
    mCurrentRealm->LoadChunk(0);
    mCurrentRealm->LoadChunk(1);
    mCurrentRealm->LoadChunk(-1);
    mCurrentRealm->LoadChunk(0);

    Loop();

    Cleanup();

    return 0;
}

//------------------------------------------------------------------------------
Game* Game::GetInstance()
{
    static Game* instance = new Game();
    return instance;
}

//------------------------------------------------------------------------------
sf::Time Game::GetTime()
{
    return mClock.getElapsedTime();
}

//------------------------------------------------------------------------------
sf::Font Game::GetFont()
{
    return mFont;
}

//------------------------------------------------------------------------------
PhysicsEngine*  Game::GetPhysicsEngine()    { return mPhysicsEngine; }
int             Game::GetWindowWidth()      { return mWindowWidth; }
int             Game::GetWindowHeight()     { return mWindowHeight; }

//------------------------------------------------------------------------------
