//==============================================================================
/*
 *  Game - MMO!
 *
 *  Ben Kelly
 */
//==============================================================================

#include "console.h"
#include "game.h"

//------------------------------------------------------------------------------
 std::string trim(const std::string& str, const std::string& whitespace = " \t")
{
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content

    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

//------------------------------------------------------------------------------
Console::Console()
{
    mConsolePosition        = 0;
    mOpen                   = false;
    mFullyOpen              = false;
    mFullyClosed            = true;
    mInputText              = "";
    mInputCursorOn          = false;
    mInputCursorToggleLast  = Game::GetInstance()->GetTime();
    mCurrentInputMode       = CI_SAY;
    mInputModeTarget        = "Scorpion";

    // Grab the font
    //mFont = Game::GetInstance()->GetFont();

    // Test character width and height
    /***
    SDL_Color textColour = {0xFF, 0xFF, 0xFF};
    SDL_Surface* textSurface = TTF_RenderText_Blended(mFont, "TEST", textColour);
    mFontCharWidth  = textSurface->w / 4;
    mFontCharHeight = textSurface->h;
    SDL_FreeSurface(textSurface);
    printf("Calculated character size at %dWx%dH\n", mFontCharWidth, mFontCharHeight);
    ***/
}

//------------------------------------------------------------------------------
Console::~Console()
{

}

//------------------------------------------------------------------------------
void Console::TruncateInput()
{
    // Remove new lines
    unsigned int i = 0;
    while (i < mInputText.size()) {
        i = mInputText.find("\n", i);
        if (i == std::string::npos) break;
        mInputText.erase(i);
    }
    i = 0;
    while (i < mInputText.size()) {
        i = mInputText.find("\r", i);
        if (i == std::string::npos) break;
        mInputText.erase(i);
    }

    // Limit input string length
    while (mInputText.size() > GAME_CONSOLE_MAX_INPUT) {
        mInputText.pop_back();
    }
}

//------------------------------------------------------------------------------
/*
 * Returns true if an input mode (/say /whisper etc) was found in the input
 */
bool Console::ParseInput()
{
    // If string is shorter than 2 then we don't care
    if (mInputText.size() < 2) return false;

    // If the first character is not '/' then we don't care
    std::string firstCharacter = mInputText.substr(0, 1);
    if (firstCharacter != "/") return false;

    // The user is typing a slash command at this point - lets deconstruct
    unsigned int firstSpacePos = mInputText.find_first_of(" ");
    std::string slashCmd = mInputText.substr(1, firstSpacePos - 1);
    std::string args;
    if (mInputText.size() <= firstSpacePos) {
        args = "";
    } else {
        args = mInputText.substr(firstSpacePos + 1, std::string::npos);
    }
    printf("Slash Command: %s\n", slashCmd.c_str());
    if (slashCmd == "s" or slashCmd == "say") {
        mCurrentInputMode = CI_SAY;
        mInputText = args;
        return true;
    }
    if (slashCmd == "w" or slashCmd == "whisper") {
        mCurrentInputMode = CI_WHISPER;
        mInputText = args;
        return true;
    }
    if (slashCmd == "r" or slashCmd == "reply") {
        mCurrentInputMode = CI_REPLY;
        mInputText = args;
        return true;
    }
    return false;
}

//------------------------------------------------------------------------------
void Console::ProcessInput()
{
    // Trim leading and trailing whitespace
    mInputText = trim(mInputText);

    if (mInputText != "") {
        mInputBuffer.push_front(mInputText);
        if (mInputBuffer.size() > GAME_CONSOLE_INPUT_BUFFER_SIZE) {
            mInputBuffer.pop_back();
        }
        if (GAME_CONSOLE_LOCAL_ECHO) {
            // push_front because we render from the bottom up.
            mOutputBuffer.push_front("> " + mInputText);
            if (mOutputBuffer.size() > GAME_CONSOLE_OUTPUT_BUFFER_SIZE) {
                mOutputBuffer.pop_back();
            }
        }
        mOutputBuffer.push_front(mInputText);
        if (mOutputBuffer.size() > GAME_CONSOLE_OUTPUT_BUFFER_SIZE) {
            mOutputBuffer.pop_back();
        }
    }
}

//------------------------------------------------------------------------------
void Console::Open()
{
    if (mOpen) {
        // Console is already open
        return;
    } else {
        mOpen = true;
    }
}

//------------------------------------------------------------------------------
void Console::Close()
{
    if (!mOpen) {
        // Console is already closed or closing
        return;
    } else {
        mOpen = false;
    }
}

//------------------------------------------------------------------------------
/**
 * Calculates the alpha value of the console background based on the current
 * position.
 *
 * @param       position of the console. can be provided for extrapolation
 * @return      The alpha value as an integer 0..255
 */
int Console::CalcAlpha(int pos)
{
    return GAME_CONSOLE_ALPHA * ((float)pos / CalcYTarget());
}

//------------------------------------------------------------------------------
/**
 * Calculates the final Y coordinate that the console will be slid down to.
 *
 * @return      the Y coordinate that the console will be rendered to
 */
int Console::CalcYTarget()
{
    return Game::GetInstance()->GetWindowHeight() * GAME_CONSOLE_TARGET_Y_PCT;
}

//------------------------------------------------------------------------------
/**
 * Calculates the new Y position for the console
 * <p>
 * This function is called by both the update and render functions
 *
 * @param ms    the numer of milliseconds to use to extrapolate the new position
 * @return      the Y coordinate that the console will be rendered to
 */
int Console::CalcYPos(float dt)
{
    if (dt == 0.0f) return mConsolePosition;
    int newPos = mConsolePosition;
    int target = CalcYTarget();
    if (mOpen) {
        // Slide the console down
        newPos = mConsolePosition + target / (GAME_CONSOLE_SPEED * dt);
        if (newPos > target) newPos = target;
    } else {
        // Slide the console up
        newPos = mConsolePosition - target / (GAME_CONSOLE_SPEED * dt);
        if (newPos < 0) newPos = 0;
    }
    return newPos;
}

//------------------------------------------------------------------------------
/**
 * Calculates the Y axis velocity necessary to reach the fully open position in
 * the set time
 *
 * @return      velocity in pixels per second
 */
double Console::CalcYVel()
{
        int distance = CalcYTarget();
        return distance * (1000.0 / GAME_CONSOLE_SPEED);
}

//------------------------------------------------------------------------------
bool Console::OnEvent(sf::Event event)
{
    if (!mOpen) {
        // Return and tell the event loop to continue processing other events
        return false;
    }

    if (event.type == sf::Event::KeyPressed) {
        if (event.key.code == sf::Keyboard::Tilde) {
            // Don't handle this, let the game close the console.
            return false;
        }
        if (event.key.code == sf::Keyboard::BackSpace && mInputText.length() > 0)
        {
            // Handle backspace
            mInputText.pop_back();
            return true;
        }
        if (event.key.code == sf::Keyboard::V && (sf::Keyboard::LControl || sf::Keyboard::LControl))
        {
            // Handle paste
            //mInputText += SDL_GetClipboardText();
            //TruncateInput();
            return true;
        }
        if (event.key.code == sf::Keyboard::C (sf::Keyboard::LControl || sf::Keyboard::LControl))
        {
            // Handle Ctrl-c to blank line
            mInputText = "";
            return true;
        }
        if (event.key.code == sf::Keyboard::Return) {
            // Enter Key
            ProcessInput();
            mInputText = "";
            return true;
        }
    }
    /***
    if (e->type == SDL_TEXTINPUT) {
        if (e->text.text[0] == ' ')
        {
            if (ParseInput()) {
                return true;
            }
        }
        if (e->text.text[0] == '`') {
            return false;
        }
        //Not pasting
        if (!((e->text.text[0] == 'v' || e->text.text[0] == 'V' ) && SDL_GetModState() & KMOD_CTRL)) {
            //Append character
            mInputText += e->text.text;
            TruncateInput();
            return true;
        }
    }
    ***/
    return false;
}

//------------------------------------------------------------------------------
/**
 *
 */
void Console::Update(float dt)
{
    int target;
    if (mOpen && !mFullyOpen) {
        target = CalcYTarget();
        mFullyClosed = false;
        mConsolePosition = CalcYPos(dt);
        if (mConsolePosition >= target) {
            mConsolePosition = target;
            mFullyOpen = true;
        }
    }
    if (!mOpen && !mFullyClosed) {
        target = CalcYTarget();
        mFullyOpen = false;
        mConsolePosition = CalcYPos(dt);
        if (mConsolePosition <= 0) {
            mConsolePosition = 0;
            mFullyClosed = true;
        }
    }
}

//------------------------------------------------------------------------------
void Console::Render(float dt, sf::RenderWindow* window)
{
    if (mFullyClosed) return;

    /***
    SDL_Color textColour;
    SDL_Surface* textSurface;
    SDL_Texture* textTexture;
    SDL_Rect inputRect;
    ***/

    int consolePosition = CalcYPos(ms);

    /***
    SDL_SetRenderDrawColor(
        renderer,
        0x00, 0x00, 0x00,
        CalcAlpha(consolePosition)
    );


    SDL_Rect conRect;
    conRect.x = 0;
    conRect.y = 0;
    conRect.w = Game::GetInstance()->GetWindowWidth();
    conRect.h = consolePosition;
    SDL_RenderFillRect(renderer, &conRect);
    ***/

    // Output Text
    int avaliableHeight = consolePosition;
    avaliableHeight    -= GAME_CONSOLE_MARGIN_BOTTOM;
    avaliableHeight    -= GAME_CONSOLE_MARGIN_TOP;
    avaliableHeight    -= mFontCharHeight; // Leave room for the input text field
    avaliableHeight    -= GAME_CONSOLE_TEXT_SPACING_VERTICAL; // Leave room for the input text field

    unsigned int availableLines = avaliableHeight / (mFontCharHeight + GAME_CONSOLE_TEXT_SPACING_VERTICAL);
    if (availableLines < 0) availableLines = 0;

    textColour = {0xCC, 0xCC, 0xCC};
    std::list<std::string>::iterator it;
    unsigned int i = 0;
    for (it = mOutputBuffer.begin(); it != mOutputBuffer.end(); it++) {
        if (mOutputBuffer.size() <= i) break; // No more lines to process
        textSurface = TTF_RenderText_Blended(mFont, (*it).c_str(), textColour);
        textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
        SDL_FreeSurface(textSurface);
        inputRect.x  = GAME_CONSOLE_MARGIN_LEFT;
        inputRect.y  = consolePosition - GAME_CONSOLE_MARGIN_BOTTOM - mFontCharHeight;
        inputRect.y -= GAME_CONSOLE_TEXT_SPACING_VERTICAL;
        inputRect.y -= mFontCharHeight * (i + 1);
        inputRect.y -= GAME_CONSOLE_TEXT_SPACING_VERTICAL * (i + 1);
        SDL_QueryTexture(textTexture, NULL, NULL, &inputRect.w, &inputRect.h);
        SDL_RenderCopy(renderer, textTexture, NULL, &inputRect);
        SDL_DestroyTexture(textTexture);
        i++;
        if (i >= availableLines) break;
    }
    // Input Text
    std::string text;
    switch (mCurrentInputMode) {
        case CI_SAY:
            text = "Say: " + mInputText;
            break;
        case CI_WHISPER:
        case CI_REPLY:
            text = "To " + mInputModeTarget + ": " + mInputText;
            break;
    }
    // Add flashing cursor
    if (SDL_GetTicks() >= mInputCursorToggleLast + GAME_CONSOLE_CURSOR_FLASH_SPEED) {
        mInputCursorToggleLast = SDL_GetTicks();
        mInputCursorOn = !mInputCursorOn;
    }
    if (mInputCursorOn) text += "_";

    // Set the text colour
    if (mCurrentInputMode == CI_SAY)     textColour = {0xFF, 0xFF, 0xFF};
    if (mCurrentInputMode == CI_WHISPER) textColour = {0xD9, 0x14, 0xA7};
    if (mCurrentInputMode == CI_REPLY)   textColour = {0xD9, 0x14, 0xA7};

    textSurface = TTF_RenderText_Blended(mFont, text.c_str(), textColour);
    textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
    SDL_FreeSurface(textSurface);
    inputRect.x = GAME_CONSOLE_MARGIN_LEFT;
    inputRect.y = consolePosition - GAME_CONSOLE_MARGIN_BOTTOM - mFontCharHeight;
    SDL_QueryTexture(textTexture, NULL, NULL, &inputRect.w, &inputRect.h);
    SDL_RenderCopy(renderer, textTexture, NULL, &inputRect);
    SDL_DestroyTexture(textTexture);
}

//------------------------------------------------------------------------------
void Console::Toggle()
{
    if (mOpen) {
        Close();
    } else {
        Open();
    }
}

//------------------------------------------------------------------------------
std::string Console::GetInputText()
{
    return mInputText;
}
