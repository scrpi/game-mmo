//==============================================================================
/*
 *  Game - MMO!
 *
 *  Ben Kelly
 */
//==============================================================================

#include "realm.h"

btRigidBody* addSphere(double rad, double x, double y, double mass)
{
	btTransform t;	//position and rotation
	t.setIdentity();
	t.setOrigin(btVector3(x, y, 0));	//put it to x,y,z coordinates
	btSphereShape* sphere = new btSphereShape(rad);	//it's a sphere, so use sphereshape
	btVector3 inertia(0, 0, 0);	//inertia is 0,0,0 for static object, else
	if(mass != 0.0)
		sphere->calculateLocalInertia(mass, inertia);	//it can be determined by this function (for all kind of shapes)

	btMotionState* motion=new btDefaultMotionState(t);	//set the position (and motion)
	btRigidBody::btRigidBodyConstructionInfo info(mass,motion,sphere,inertia);	//create the constructioninfo, you can create multiple bodies with the same info
	info.m_restitution = 0.3f;
    info.m_friction = 0.0f;
	btRigidBody* body=new btRigidBody(info);	//let's create the body itself
	body->setLinearFactor(btVector3(1,1,0));
    body->setAngularFactor(btVector3(0,0,1));
	return body;
}


//------------------------------------------------------------------------------
Realm::Realm(int id)
{
    mID = id;

    mBTCollisionConfiguration = new btDefaultCollisionConfiguration();

    mBTDispatcher = new btCollisionDispatcher(mBTCollisionConfiguration);

    mBTBroadphase = new btDbvtBroadphase();

    mBTSolver = new btSequentialImpulseConstraintSolver;

    mBTWorld = new btDiscreteDynamicsWorld(
        mBTDispatcher, mBTBroadphase, mBTSolver, mBTCollisionConfiguration
    );

    mBTWorld->setGravity(btVector3(0, 9.8, 0));

    // Add a plane
    btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0,-1,0),0);
    btTransform groundTransform;
	groundTransform.setIdentity();
	groundTransform.setOrigin(btVector3(0.0f,24.0f,0.0f));
	btScalar mass(0.);
    btVector3 localInertia(0,0,0);
    btDefaultMotionState* myMotionState = new btDefaultMotionState(groundTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,groundShape,localInertia);
    rbInfo.m_restitution = 0.0f;
    rbInfo.m_friction = 0.0f;
    btRigidBody* body = new btRigidBody(rbInfo);
    mBTWorld->addRigidBody(body);

    for (float i = 0; i < 6; i++) {
        for (float j = 0; j < 15; j++) {
            mBodies.push_back(addSphere(0.4, i + 5, j - 2, 1.0)); // rad, x, y, mass, world
        }
    }

    mBodies.push_back(addSphere(0.9, 7.5, -35.0, 10.0)); // rad, x, y, mass, world
    ////mBodies.push_back(addSphere(0.9, 8, -35.0, 5.0)); // rad, x, y, mass, world
    ////mBodies.push_back(addSphere(0.9, 10, -35.0, 5.0)); // rad, x, y, mass, world

 	for(int i=0;i<mBodies.size();i++)
	{
		mBTWorld->addRigidBody(mBodies[i]);
	}
}

//------------------------------------------------------------------------------
void Realm::Update(float dt)
{
    mBTWorld->stepSimulation(dt, 1000, 1.0f / 60.0f);
    printf("Physics Step: %f\n", dt);
}

//------------------------------------------------------------------------------
void Realm::Render(float dt, sf::RenderWindow* window)
{
	for(int i=0;i<mBodies.size();i++)
	{
		if(mBodies[i]->getCollisionShape()->getShapeType()==SPHERE_SHAPE_PROXYTYPE) {
            btRigidBody* body = mBodies[i];
            int rad = (int)(((btSphereShape*)body->getCollisionShape())->getRadius() * 16);
            btTransform t;
            body->getMotionState()->getWorldTransform(t);	//get the transform
            btVector3 origin = t.getOrigin();

            int mPX = (int)(origin.x() * 16);
            int mPY = (int)(origin.y() * 16 );

            sf::CircleShape circ(rad);
            circ.setFillColor(sf::Color::Black);
            circ.setOutlineThickness(1);
            circ.setOutlineColor(sf::Color::White);
            circ.setPosition(mPX, mPY);
            window->draw(circ);

            /****
            //SDL_SetRenderDrawColor(renderer, 128, 128, 255, 255);
            //SDL_RenderDrawPoint(renderer, mPX,  mPY);

            // Draw circle

            int x = rad;
            int y = 0;
            int radiusError = 1 - rad;

            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
            while (x >= y) {
                SDL_RenderDrawPoint(renderer,  x + mPX,  y + mPY);
                SDL_RenderDrawPoint(renderer,  y + mPX,  x + mPY);
                SDL_RenderDrawPoint(renderer, -x + mPX,  y + mPY);
                SDL_RenderDrawPoint(renderer, -y + mPX,  x + mPY);
                SDL_RenderDrawPoint(renderer, -x + mPX, -y + mPY);
                SDL_RenderDrawPoint(renderer, -y + mPX, -x + mPY);
                SDL_RenderDrawPoint(renderer,  x + mPX, -y + mPY);
                SDL_RenderDrawPoint(renderer,  y + mPX, -x + mPY);
                y++;
                if (radiusError < 0) {
                    radiusError += 2 * y + 1;
                } else {
                    x--;
                    radiusError += 2 * (y - x + 1);
                }
            }
            */
		}
	}
}

//------------------------------------------------------------------------------
void Realm::LoadChunk(int chunkX)
{
    // Is the chunk already in the chunk map? If so we do nothing.
    std::map<int, Chunk*>::const_iterator it = mChunkMap.find(chunkX);
    if (it != mChunkMap.end()) {
        printf("Chunk %d already in map\n", chunkX);
        return;
    }

    mChunkMap[chunkX] = new Chunk(chunkX, mID);
    mChunkMap[chunkX]->LoadFromFile();
    printf("Chunk %d Loaded\n", chunkX);
}
