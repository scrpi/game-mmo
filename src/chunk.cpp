//==============================================================================
/*
 *  Game - MMO!
 *
 *  Ben Kelly
 */
//==============================================================================

#include "chunk.h"

//------------------------------------------------------------------------------
Chunk::Chunk(int chunkX, int realmID)
{
    mChunkX = chunkX;
    mRealmID = realmID;

    std::stringstream sstrm;
    sstrm << "chunks/" << mRealmID << "_" << mChunkX << ".cdf";
    mChunkDataFilePath = sstrm.str();
}

//------------------------------------------------------------------------------
void Chunk::LoadFromFile()
{
    printf("Loading CDF %s\n", mChunkDataFilePath.c_str());
    for (int i = 0; i < GAME_CHUNK_L; i++){
        mTileIndex[i] = 0;
        mPassableIndex[i] = true;
    }
}
