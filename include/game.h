//==============================================================================
/*
 *  Game - MMO!
 *
 *  Ben Kelly
 */
//==============================================================================
#ifndef __GAME_H__
#define __GAME_H__

#include <cstdio>

#include <SDL2/SDL.h>
#include <SDL2/SDL_Image.h>
#include <SDL2/SDL_ttf.h>

#include <SFML/Graphics.hpp>

#include "console.h"
#include "realm.h"
#include "physics.h"

const int GAME_TICKS_PER_SECOND = 20;

// Interval in ms to calculate the FPS
const int GAME_FPS_CALC_INTERVAL = 500;

const int GAME_FONT_SIZE = 11;

//------------------------------------------------------------------------------
class Game
{
private:
    // Singleton
    // http://gameprogrammingpatterns.com/singleton.html
    Game() {}
    ~Game() {}
    /*
    SDL_Window*             mWindow;
    SDL_Renderer*           mRenderer;
    SDL_GLContext           mSDLGLContext;
    */
    sf::RenderWindow        mWindow;
    sf::Clock               mClock;
    sf::Clock               mFPSClock;
    sf::Font                mFont;
    sf::Texture             mBGTex;
    sf::RectangleShape      mBGRect;
    PhysicsEngine*          mPhysicsEngine;
    int                     mWindowWidth;
    int                     mWindowHeight;
    bool                    mRunning;
    Console*                mConsole;
    float                   mLastTicks;
    //SDL_Texture*            mBG;
    int                     mLastFPSCalc;
    int                     mFPS;
    int                     mFrameCount;
    //TTF_Font*               mFont;
    int                     mCurrentRealmID;
    Realm*                  mCurrentRealm;

    bool                    InitOpenGL();
    bool                    Init();
    void                    RenderFPS();
    void                    OnEvent(sf::Event event);
    void                    Update(float dt);
    void                    Render(float dt);
    void                    Loop();
    void                    Cleanup();

public:
    int                     Execute(int argc, char* argv[]);
    static Game*            GetInstance();
    SDL_Renderer*           GetRenderer();
    PhysicsEngine*          GetPhysicsEngine();
    sf::Time                GetTime();
    sf::Font                GetFont();
    int                     GetWindowWidth();
    int                     GetWindowHeight();
};

#endif // __GAME_H__
