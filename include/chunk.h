//==============================================================================
/*
 *  Game - MMO!
 *
 *  Ben Kelly
 */
//==============================================================================
#ifndef __CHUNK_H__
#define __CHUNK_H__

#include <cstdio>
#include <sstream>
#include <string>

const int GAME_CHUNK_W          = 16;
const int GAME_CHUNK_H          = 256;
const int GAME_CHUNK_L          = GAME_CHUNK_W * GAME_CHUNK_H;

//------------------------------------------------------------------------------
class Chunk
{
private:
    int                     mChunkX;
    int                     mRealmID;
    char                    mTileIndex[GAME_CHUNK_L];
    bool                    mPassableIndex[GAME_CHUNK_L];
    std::string             mChunkDataFilePath;

public:
    Chunk(int, int);
    void                    LoadFromFile();
};

#endif // __CHUNK_H__