//==============================================================================
/*
 *  Game - MMO!
 *
 *  Ben Kelly
 */
//==============================================================================
#ifndef __PHYSICS_H__
#define __PHYSICS_H__

#include <cmath>
#include <cstdio>

enum CollisionGroup {
    GAME_COL_GROUP_CONSOLE,
    GAME_COL_GROUP_REALM,
};

enum CollisionShape {
    GROUP_COL_SHAPE_RECT,
    GROUP_COL_SHAPE_CIRC,
};

//------------------------------------------------------------------------------
/**
 * Vec2 holds 2 dimentional position in real coordinates
 */
class Vec2
{
public:
    double          x;
    double          y;

    Vec2();
    Vec2(double _x, double _y);
    Vec2            operator+(const Vec2 &other);
    Vec2            operator*(double scalar);
    Vec2            DistTo(Vec2 target);
    bool            Inside(Vec2 prev, Vec2 next);

    static const Vec2 ZERO;
};

class PhysicsComponent;

//------------------------------------------------------------------------------
class PhysicsEngine
{
public:
    PhysicsEngine();
    void            RegisterComponent(PhysicsComponent* component, CollisionGroup colGroup);
};

//------------------------------------------------------------------------------
class PhysicsComponent
{
private:
    PhysicsEngine*  mEng;
    Vec2            mPos;               // Position in pixels
    Vec2            mThr;               // Thrust   - Pixels per second per second
    Vec2            mVel;               // Velocity - Pixels per second
    Vec2            mGrv;               // Gravity  - Pixels per second per second
    double          mBounce;

public:
    PhysicsComponent(CollisionGroup colGroup);
    Vec2            GetPosition();
    Vec2            GetThrust();
    Vec2            GetVelocity();
    Vec2            GetGravity();
    double          GetBounce();
    void            SetPosition(Vec2 pos);
    void            SetThrust(Vec2 thrust);
    void            SetVelocity(Vec2 vel);
    void            SetGravity(Vec2 gravity);
    void          SetBounce(double bounce);
    void            Update(int ms);
    Vec2            IntegrateEuler(int ms);               // Returns new position vector

};

#endif // __PHYSICS_H__
