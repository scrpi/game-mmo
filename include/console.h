//==============================================================================
/*
 *  Game - MMO!
 *
 *  Ben Kelly
 */
//==============================================================================
#ifndef __CONSOLE_H__
#define __CONSOLE_H__

#include <cstdio>
#include <cmath>
#include <string>
#include <list>
#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "physics.h"

// Time in ms for the console to open
const unsigned int GAME_CONSOLE_SPEED                    = 250;

// Max input text in characters
const unsigned int GAME_CONSOLE_MAX_INPUT                = 110;

// The Y level as a percentage that the console will slide down to
const float GAME_CONSOLE_TARGET_Y_PCT           = 0.4;

// The alpha value for the consle at fully open
const float GAME_CONSOLE_ALPHA                  = 192;

// Number of input strings to keep stored
const unsigned int GAME_CONSOLE_INPUT_BUFFER_SIZE        = 500;

// Number of output strings to keep stored
const unsigned int GAME_CONSOLE_OUTPUT_BUFFER_SIZE       = 200;

const unsigned int GAME_CONSOLE_MARGIN_LEFT              = 5;
const unsigned int GAME_CONSOLE_MARGIN_TOP               = 0;
const unsigned int GAME_CONSOLE_MARGIN_BOTTOM            = 5;
const unsigned int GAME_CONSOLE_TEXT_SPACING_VERTICAL    = 0;

// Time in ms to flash the console cursor
const int unsigned GAME_CONSOLE_CURSOR_FLASH_SPEED       = 300;

const bool GAME_CONSOLE_LOCAL_ECHO              = false;

//------------------------------------------------------------------------------
enum ConsoleInputMode {
    CI_SAY,
    CI_WHISPER,
    CI_REPLY,
};

//------------------------------------------------------------------------------
class Console
{
private:
    int                     mConsolePosition;
    bool                    mOpen;
    bool                    mFullyOpen;
    bool                    mFullyClosed;
    std::string             mInputText;
    int                     mFontCharWidth;
    int                     mFontCharHeight;
    std::list<std::string>  mInputBuffer;
    std::list<std::string>  mOutputBuffer;
    bool                    mInputCursorOn;
    Uint32                  mInputCursorToggleLast;
    ConsoleInputMode        mCurrentInputMode;
    std::string             mInputModeTarget;

    void                    TruncateInput();
    bool                    ParseInput();
    void                    ProcessInput();
    void                    Open();
    void                    Close();
    int                     CalcAlpha(int);
    int                     CalcYTarget();
    int                     CalcYPos(int);
    double                  CalcYVel();

public:
    Console();
    ~Console();
    bool                    OnEvent(sf::Event event);
    void                    Update(float dt);
    void                    Render(float dt, sf::RenderWindow* window);
    void                    Toggle();
    std::string             GetInputText();
};

#endif // __CONSOLE_H__
