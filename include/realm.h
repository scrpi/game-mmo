//==============================================================================
/*
 *  Game - MMO!
 *
 *  Ben Kelly
 */
//==============================================================================
#ifndef __REALM_H__
#define __REALM_H__

#include <cstdio>
#include <map>
#include <stdio.h>
#include <time.h>

#include <SFML/Graphics.hpp>
#include <SDL2/SDL.h>
#include "btBulletDynamicsCommon.h"

#include "chunk.h"

//------------------------------------------------------------------------------
class Realm
{
private:
    int                                     mID;
    std::map<int, Chunk*>                   mChunkMap;
	btAlignedObjectArray<btRigidBody*>	    mBodies;
	btCollisionConfiguration*               mBTCollisionConfiguration;
	btCollisionDispatcher*	                mBTDispatcher;
	btBroadphaseInterface*	                mBTBroadphase;
	btConstraintSolver*	                    mBTSolver;
	btDynamicsWorld*                        mBTWorld;

public:
    Realm(int);
    void                    Update(float dt);
    void                    Render(float dt, sf::RenderWindow* window);
    void                    LoadChunk(int chunkX);
};

#endif // __REALM_H__
